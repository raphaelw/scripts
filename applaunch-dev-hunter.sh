#!/bin/bash
sleep 0.5
~/devbox/scripts/check_services.sh

#Chrome
killall chrome
nohup /usr/bin/google-chrome-stable %U --auto-open-devtools-for-tabs https://hunter.dev.jungwild.io >/dev/null 2>&1 &
disown

#Firefox
#killall firefox-bin
nohup firefox https://www.google.com/ >/dev/null 2>&1 &
disown

# PhpStorm
nohup /opt/PhpStorm-223.8617.59/bin/phpstorm.sh >/dev/null ~/dev 2>&1 &
disown
sleep 1
