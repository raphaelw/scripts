#!/bin/bash
sleep 0.1
# Alle Fenster Minimieren
clear
echo "minimize all windows"
~/scripts/wmctrl-minimize-all.sh

clear
echo "get terminal"

#Terminal wieder in den focus
WID=$(wmctrl -lx | grep 'gnome-terminal-server.Gnome-terminal' | awk '{print $1}' )
wmctrl -i -a "$WID"

sleep 0.1
clear

echo "positioniere Fenster"

for i in {1..2}
do
    # PHPSTORM extension
    WID=$(wmctrl -lx | grep 'jetbrains-phpstorm.jetbrains-phpstorm  raphaels-denkmatte hunter-chrome-ext' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,1168,0,1168,1363

    #phpstorm backend
    WID=$(wmctrl -lx | grep 'jetbrains-phpstorm.jetbrains-phpstorm  raphaels-denkmatte dev' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,0,0,1168,1363

    # Google Chrome
    WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte jungwild Hunter' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,23111,0,1148,644
    #wmctrl -i -a "$WID"
    #xdotool getwindowfocus windowmove -- -20 0

    # Chrome DevTools
    WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte DevTools' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,23111,640,1104,763
    sleep 1
done




clear
echo "ready.."
sleep 1
