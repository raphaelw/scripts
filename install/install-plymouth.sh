#!/bin/bash
sudo apt install git -y
cd ~
git clone https://github.com/anaysharma/nibar-plymouth-theme.git
cd nibar-plymouth-theme
sudo cp -r nibar /usr/share/plymouth/themes/
sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/nibar/nibar.plymouth 100
sudo update-alternatives --set default.plymouth /usr/share/plymouth/themes/nibar/nibar.plymouth
sudo update-initramfs -u
