#!/bin/bash

# Alle Fenster Minimieren
sleep 0.1
clear
echo "minimize windows"
# Alle Fenster nach links und Minimieren
~/scripts/wmctrl-minimize-all.sh

clear
echo "get terminal"


for i in {1..2}
do
    #Terminal wieder in den focus
    #WID=$(wmctrl -lx | grep 'gnome-terminal-server.Gnome-terminal' | awk '{print $1}' )
    #wmctrl -i -a "$WID"
    #wmctrl -i -a "$WID"

    clear
    echo "positioniere fenster"
    sleep 0.1
    # PHPSTORM
    WID=$(wmctrl -lx | grep 'jetbrains-phpstorm.jetbrains-phpstorm  raphaels-denkmatte dev' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,0,0,1168,1363


    # Google Chrome
    WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte jungwild Hunter' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,1147,0,1167,1444
    #wmctrl -i -a "$WID"
    #xdotool getwindowfocus windowmove -- -20 0

    # CHrome DevTools
    #WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte DevTools' | awk '{print $1}' )
    #wmctrl -i -a "$WID"
    #wmctrl -i -r "$WID" -e 0,1168,640,1125,763

    # Firefox
    WID=$(wmctrl -lx | grep 'Navigator.firefox' | awk '{print $1}' )
    wmctrl -i -a "$WID"
    wmctrl -i -r "$WID" -e 0,2267,0,1199,1452
    sleep 1
done



