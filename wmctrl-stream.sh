#!/bin/bash
sleep 0.1
echo "checke services"
~/devbox/scripts/check_services.sh
clear

echo "minimize windows"
# Alle Fenster nach links und Minimieren
~/scripts/wmctrl-minimize-all.sh

clear
echo "focus windows.."
sleep 0.1
# PHPSTORM hunter
WID=$(wmctrl -lx | grep 'jetbrains-phpstorm.jetbrains-phpstorm  raphaels-denkmatte dev' | awk '{print $1}' )
wmctrl -i -a "$WID"
wmctrl -i -r "$WID" -e 0,1520,0,1920,1070

# PHPSTORM extension
WID=$(wmctrl -lx | grep 'jetbrains-phpstorm.jetbrains-phpstorm  raphaels-denkmatte hunter-chrome-ext' | awk '{print $1}' )
wmctrl -i -a "$WID"
wmctrl -i -r "$WID" -e 0,1520,0,1920,1070

# Terminal
WID=$(wmctrl -lx | grep 'gnome-terminal-server.Gnome-terminal' | awk '{print $1}' )
wmctrl -i -a "$WID"
wmctrl -i -r "$WID" -e 0,1520,0,1920,1070

#DevTools
WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte DevTools' | awk '{print $1}' )
wmctrl -i -a "$WID"
wmctrl -i -r "$WID" -e 0,1520,0,1920,1033

# Chrome
WID=$(wmctrl -lx | grep 'google-chrome.Google-chrome  raphaels-denkmatte jungwild Hunter' | awk '{print $1}' )
wmctrl -i -a "$WID"
wmctrl -i -r "$WID" -e 0,1520,0,1920,1070
wmctrl -i -a "$WID"

clear
echo "ready"
sleep 1
